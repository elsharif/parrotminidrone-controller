//https://github.com/poshaughnessy/web-bluetooth-parrot-drone
//https://github.com/voodootikigod/node-rolling-spider/
//https://www.mathworks.com/videos/programming-drones-with-simulink-1513024653640.html

'use strict';

const WriteWithoutResponseTag = 'fa00';
const NotifyTag = 'fb00';
const ReadWriteNotifyTag = 'fd21';
const ReadWriteNotify2Tag = 'fd51';
// fc00 - contains 'write' characteristic ffc1, not currently used
// fe00 - contains characteristics fe01, fe02, not currently used
 

const DEFAULT_SPEED = 10;
const DEFAULT_DRIVE_STEPS = 40;

let ParrotDrone = function() {
  let droneDevice = null,
    gattServer = null,
    // Used to store the 'counter' that's sent to each characteristic
    steps = {
      'fa0a': 1,
      'fa0b': 1,
      'fa0c': 1
    },
    speeds = {
      yaw: 0,     // turn
      pitch: 0,   // forward/backward
      roll: 0,    // left/right
      altitude: 0 // up/down
    },
    ping = null,
    driveStepsRemaining = 0,
    services = {},
    characteristics = {},
    onDisconnectCallback = null;

  function _getUUID(uniqueSegment) {
    return '9a66' + uniqueSegment + '-0800-9191-11e4-012d1540cb8e';
  }

  function _startNotificationsForCharacteristic(serviceID, characteristicID) {
    return new Promise((resolve, reject) => {
      return _getCharacteristic(serviceID, characteristicID)
        .then(characteristic => {
          characteristic.startNotifications()
            .then(() => {
              characteristic.addEventListener('characteristicvaluechanged', event => {
                const array = new Uint8Array(event.target.value.buffer);
                let a = [];
                for (let i = 0; i < array.byteLength; i++) {
                  a.push('0x' + ('00' + array[i].toString(16)).slice(-2));
                }
                if (characteristicID === 'fb0e') {
                  var eventList = ['fsLanded', 'fsTakingOff', 'fsHovering', 'fsUnknown', 'fsLanding', 'fsCutOff'];
                  
                  // if (eventList[array[6]] === 'fsHovering') {
                  //   console.log('Hovering - ready to go');
                  // } else {
                  //   console.log('Not hovering... Not ready', array[6]);
                  // }

                  // if ([1, 2, 3, 4].indexOf(array[6]) >= 0) {
                  //   console.log('Flying');
                  // }
                  // else {
                  //   console.log('Not flying');
                  // }

                } else if (characteristicID === 'fb0f') {
                  const batteryLevel = array[array.length - 1];
                  console.log(`Battery Level: ${batteryLevel}%`);
                  if (batteryLevel < 10) {
                    console.error('Battery level too low!');
                  }
                }
              });
              resolve();
            });
        })
        .catch(error => {
          console.error('startNotifications error', error);
          reject();
        });
    });
  }

  function _discover() {
    return navigator.bluetooth.requestDevice({
        filters: [{ namePrefix: 'Mambo_' }],
        optionalServices: [ _getUUID(WriteWithoutResponseTag), _getUUID(NotifyTag), _getUUID(ReadWriteNotifyTag), _getUUID(ReadWriteNotify2Tag)]
      })
      .then((device) => droneDevice = device);
  }

  function _connectGATT() {
    droneDevice.addEventListener('gattserverdisconnected', _handleDisconnect);
    return droneDevice.gatt.connect().then(server => gattServer = server);
  }

  function _reset() {
    ping = null;
    driveStepsRemaining = 0;
    services = {};
    characteristics = {};
    onDisconnectCallback = null;
  }

  function _handleDisconnect() {
    if (onDisconnectCallback && typeof onDisconnectCallback === 'function') {
      onDisconnectCallback();
    }
    _reset();
    droneDevice.removeEventListener('gattserverdisconnected', _handleDisconnect);
  }

  function _getService(serviceID) {
    return new Promise((resolve, reject) => {
      const service = services[serviceID];
      if (service) {
        resolve(service);
      } else {
        return gattServer.getPrimaryService(_getUUID(serviceID))
          .then(service => {
            services[serviceID] = service;
            resolve(service);
          })
          .catch(reject);
      }
    });
  }

  function _getCharacteristic(serviceID, characteristicID) {
    return new Promise((resolve, reject) => {
      const char = characteristics[characteristicID];
      if (char) {
        resolve(char);
      } else {
        return _getService(serviceID)
          .then(service => service.getCharacteristic( _getUUID(characteristicID)))
          .then(characteristic => {
            characteristics[characteristicID] = characteristic;
            resolve(characteristic);
          })
          .catch(reject);
      }
    })
  }

  function _writeCommand(characteristic, commandArray) {
    var buffer = new ArrayBuffer(commandArray.length);
    var command = new Uint8Array(buffer);
    command.set(commandArray);
    //console.log('Write command', command);
    return characteristic.writeValue(command);
  }

  function _writeTo(serviceID, characteristicID, commandArray) {
    return _getCharacteristic(serviceID, characteristicID).then(characteristic => _writeCommand(characteristic, commandArray));
  }

  function _startNotifications() {
    return _startNotificationsForCharacteristic(NotifyTag, 'fb0f')
      .then(() => _startNotificationsForCharacteristic(NotifyTag, 'fb0e'))
      .then(() => _startNotificationsForCharacteristic(NotifyTag, 'fb1b'))
      .then(() => _startNotificationsForCharacteristic(NotifyTag, 'fb1c'))
      .then(() => _startNotificationsForCharacteristic(ReadWriteNotifyTag, 'fd22'))
      .then(() => _startNotificationsForCharacteristic(ReadWriteNotifyTag, 'fd23'))
      .then(() => _startNotificationsForCharacteristic(ReadWriteNotifyTag, 'fd24'))
      .then(() => _startNotificationsForCharacteristic(ReadWriteNotify2Tag, 'fd52'))
      .then(() => _startNotificationsForCharacteristic(ReadWriteNotify2Tag, 'fd53'))
      .then(() => _startNotificationsForCharacteristic(ReadWriteNotify2Tag, 'fd54'))
      .then(() => console.log('Finished starting notifications'))
      .catch(error => console.error('Failed to start notifications', error));
  }

  function _handshake() {
    return droneDevice.gatt.connect() .then(() => _writeTo(WriteWithoutResponseTag, 'fa0b', [4, ++steps.fa0b, 0, 4, 1, 0, 0x32, 0x30, 0x31, 0x34, 0x2D, 0x31, 0x30, 0x2D, 0x32, 0x38, 0x00]));
  }

  function _hover() {
    driveStepsRemaining = 0;
    speeds.yaw = 0;
    speeds.pitch = 0;
    speeds.roll = 0;
    speeds.altitude = 0;
  }

  // function _startPing() {
    // ping = setInterval(() => {
    //   _writeTo(WriteWithoutResponseTag, 'fa0a',
    //     [
    //       2,
    //       ++steps.fa0a,
    //       2,
    //       0,
    //       2,
    //       0,
    //       driveStepsRemaining ? 1 : 0,
    //       speeds.roll,
    //       speeds.pitch,
    //       speeds.yaw,
    //       speeds.altitude,
    //       0,
    //       0,
    //       0,
    //       0,
    //       0,
    //       0,
    //       0,
    //       0
    //     ])
    //     .catch(_onBluetoothError);

    //   if (driveStepsRemaining > 0) {
    //     driveStepsRemaining--;
    //     if (driveStepsRemaining === 0) {
    //       _hover();
    //     }
    //   }
    // }, 50);
  // }

  const props = ['yaw', 'pitch', 'roll', 'altitude'];
  function _setSpeed(property, speed, driveSteps) {
    for (let i=0; i < props.length; i++)
      speeds[props[i]] = property === props[i] ? speed:  0;
    driveStepsRemaining = driveSteps;
  }

  function _onBluetoothError(err) {
    console.error('Error!', err);
    clearInterval(ping);
  }

  return {
    connect: disconnectCallback => {
      onDisconnectCallback = disconnectCallback;
      return new Promise((resolve) => _discover()
          .then(() => _connectGATT())
          .then(() => _startNotifications())
          .then(() => new Promise(resolve => setTimeout(() => _handshake().then(resolve), 100)))
          .then(() => resolve()));
    },
    disconnect: () => droneDevice.gatt.disconnect(),
    takeOff: () => droneDevice.gatt.connect()
        .then(() => _writeTo(WriteWithoutResponseTag, 'fa0b', [2, ++steps.fa0b & 0xFF, 2, 0, 0, 0])) // "Flat trim" which you are meant to call before taking off
        .then(() =>_writeTo(WriteWithoutResponseTag, 'fa0b', [4, ++steps.fa0b, 2, 0, 1, 0])) // Actual command to take off
        // .then(() => _startPing())
        .catch(_onBluetoothError),
    flip: () => droneDevice.gatt.connect()
        .then(() => _writeTo(WriteWithoutResponseTag, 'fa0b', [4, ++steps.fa0b, 2, 4, 0, 0, 2, 0, 0, 0]))
        .catch(_onBluetoothError),
    land: () =>  droneDevice.gatt.connect()
        .then(() => _writeTo(WriteWithoutResponseTag, 'fa0b', [4, ++steps.fa0b, 2, 0, 3, 0]))
        .then(() => clearInterval(ping))
        .catch(_onBluetoothError),
    emergencyCutOff: () => droneDevice.gatt.connect()
        .then(() => _writeTo(WriteWithoutResponseTag, 'fa0c', [0x02, ++steps.fa0c & 0xFF, 0x02, 0x00, 0x04, 0x00]))
        .then(() => clearInterval(ping))
        .catch(_onBluetoothError),
    hover: () => _hover(),
    takepicture: () => droneDevice.gatt.connect()
        .then(() => _writeTo(WriteWithoutResponseTag, 'fa0b', [0x02, ++this.steps.fa0b & 0xFF, 0x02, 0x06, 0x00, 0x00, 0x00]))
        .then(() => clearInterval(ping))
        .catch(_onBluetoothError),

    moveForwards: function() { _setSpeed('pitch', DEFAULT_SPEED, DEFAULT_DRIVE_STEPS); },
    moveBackwards: function() { _setSpeed('pitch', -DEFAULT_SPEED, DEFAULT_DRIVE_STEPS); },

    moveLeft: function() { _setSpeed('yaw', -DEFAULT_SPEED, DEFAULT_DRIVE_STEPS); },
    moveRight: function() { _setSpeed('yaw', DEFAULT_SPEED, DEFAULT_DRIVE_STEPS); },

    up: function() { _setSpeed('altitude', DEFAULT_SPEED, DEFAULT_DRIVE_STEPS); },
    down: function() { _setSpeed('altitude', -DEFAULT_SPEED, DEFAULT_DRIVE_STEPS); },

    left: function() { _setSpeed('roll', -DEFAULT_SPEED, DEFAULT_DRIVE_STEPS); },
    right: function() { _setSpeed('roll', DEFAULT_SPEED, DEFAULT_DRIVE_STEPS); },
  };
}

function connect() {
  retryCalling(10, 4, () => bluetoothDevice.gatt.connect());
}

function retryCalling(max, delay, toTry, success, fail) {
  toTry().then(result => success(result)).catch(_ => {
    if (max === 0) return fail();
    setTimeout(() => retryCalling(--max, delay * 2, toTry, success, fail), delay * 1000);
  });
}