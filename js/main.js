(function() {
  const STATE_IDLE = 'IDLE';
  const STATE_CONNECTING = 'CONNECTING';
  const STATE_CONNECTED = 'CONNECTED';
  const STATE_DISCONNECTING = 'DISCONNECTING';

  let state = STATE_IDLE;
  let connectButton;

  function setupUI() {
    connectButton = document.getElementById('connectBtn');
    let drone = ParrotDrone(),

    takeOffButton = document.getElementById('takeOffBtn'),
    forwardButton = document.getElementById('forwardBtn'),
    backwardButton = document.getElementById('backwardBtn'),
    leftButton = document.getElementById('leftBtn'),
    rightButton = document.getElementById('rightBtn'),
    hoverButton = document.getElementById('hoverBtn'),
    flipButton = document.getElementById('flipBtn'),
    landButton = document.getElementById('landBtn'),
    emergencyButton = document.getElementById('emergencyBtn');
      
    upButton = document.getElementById('upBtn'),
    downButton = document.getElementById('downBtn'),
    left = document.getElementById('left'),
    right = document.getElementById('right'),

    takepicture = document.getElementById('takepicture'),

    takeOffButton.addEventListener('click', drone.takeOff);
    forwardButton.addEventListener('click', drone.moveForwards);
    backwardButton.addEventListener('click', drone.moveBackwards);
    leftButton.addEventListener('click', drone.moveLeft);
    rightButton.addEventListener('click', drone.moveRight);
    hoverButton.addEventListener('click', drone.hover);
    flipButton.addEventListener('click', drone.flip);
    landButton.addEventListener('click', drone.land);
    emergencyButton.addEventListener('click', drone.emergencyCutOff);

    upButton.addEventListener('click', drone.up);
    downButton.addEventListener('click', drone.down);
    
    left.addEventListener('click', drone.left);
    right.addEventListener('click', drone.right);

    takepicture.addEventListener('click', drone.takepicture);
    
    connectButton.addEventListener('click', () => connect(drone));
  }

  function connect(drone){
    updateConnectionState(STATE_CONNECTING);
    drone.connect(()=> updateConnectionState(STATE_IDLE))
      .then(() => updateConnectionState(STATE_CONNECTED))
      .catch(() => updateConnectionState(STATE_IDLE));
  }

  function updateConnectionState(newState) {
    state = newState;
    connectButton.innerHTML = getConnectButtonText(state);
    switch (state) {
      case STATE_CONNECTED:
        connectButton.classList.add('connected');
        connectButton.classList.remove('pending');
        break;
      case STATE_CONNECTING:
      case STATE_DISCONNECTING:
        connectButton.classList.add('pending');
        connectButton.classList.remove('connected');
        break;
      default:
        connectButton.classList.remove('pending');
        connectButton.classList.remove('connected');
    }
  }

  function getConnectButtonText(state) {
    switch (state) {
      case STATE_CONNECTED:
        return 'DISCONNECT';
      case STATE_CONNECTING:
        return 'CONNECTING';
      case STATE_DISCONNECTING:
        return 'DISCONNECTING';
      default:
        return 'CONNECT';

    }
  }
  setupUI();
})();
